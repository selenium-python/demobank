import unittest
import time
from selenium import webdriver
from data import loginPassworGenerator
from wait import wait



class LoginPageTests(unittest.TestCase):

    @classmethod
    def setUp(self):
        self.base_url = 'https://demobank.jaktestowac.pl'
        self.driver = webdriver.Chrome(executable_path=r"/home/marcin/Pobrane/chromedriver2.41/chromedriver")

    @classmethod
    def tearDown(self):
        self.driver.quit()

    def test_correct_login_page(self):
        driver = self.driver
        driver.get(self.base_url)
        title = driver.title
        print(f'Actual title: {title}')
        self.assertEqual('Demobank - Bankowość Internetowa - Logowanie', title,
                         f'Expected title differ from actual for page url: {self.base_url}')

    def test_incorrect_login(self):
        driver = self.driver
        driver.get(self.base_url)
        # finding login input box and sending value
        login_input_element = driver.find_element_by_xpath('//*[@id="login_id"]')
        login_input_element.send_keys(loginPassworGenerator.password_generator(6))
        login_input_element.submit()
        error_element = driver.find_element_by_xpath('// div[@class ="error"]')
        error_element_text = error_element.text
        self.assertEqual('identyfikator ma min. 8 znaków', error_element_text,
                         f'Expected login button text differ from actual: {error_element_text}')

    def test_incorrect_password(self):
        driver = self.driver
        driver.get(self.base_url)
        # finding login input box and sending value
        login_input_element = driver.find_element_by_xpath('//*[@id="login_id"]')
        login_input_element.send_keys(loginPassworGenerator.password_generator(8))

        wait.wait_for_elements(driver, '//*[@id="login_next"]')
        button_next_element = driver.find_element_by_xpath('//*[@id="login_next"]')
        button_next_element.click()

        time.sleep(2)
        # finding login password box and sending value
        wait.wait_for_elements(driver, '//*[@id="login_password"]')
        password_input_element = driver.find_element_by_xpath('//*[@id="login_password"]')
        password_input_element.send_keys(loginPassworGenerator.password_generator(4))
        password_input_element.submit()
        error_element = driver.find_element_by_xpath('// div[@class ="error"]')
        error_element_text = error_element.text
        self.assertEqual('hasło ma min. 8 znaków', error_element_text,
                         f'Expected login button text differ from actual: {error_element_text}')

    def test_correct_login_and_password(self):
        driver = self.driver
        driver.get(self.base_url)
        # finding login input box and sending value
        login_input_element = driver.find_element_by_xpath('//*[@id="login_id"]')
        login_input_element.send_keys(loginPassworGenerator.password_generator(8))

        wait.wait_for_elements(driver, '//*[@id="login_next"]')
        button_next_element = driver.find_element_by_xpath('//*[@id="login_next"]')
        button_next_element.click()

        # finding login password box and sending value
        wait.wait_for_elements(driver, '//*[@id="login_password"]')
        password_input_element = driver.find_element_by_xpath('//*[@id="login_password"]')
        password_input_element.send_keys(loginPassworGenerator.password_generator(8))

        # finding button 'zaloguj'
        wait.wait_for_elements(driver, '//*[@id="login_next"]')
        button_next_element = driver.find_element_by_xpath('//*[@id="login_next"]')
        button_next_element.click()

        wait.wait_for_elements(driver, '//*[@id="show_messages"]')
        messages_element = driver.find_element_by_xpath('//*[@id="show_messages"]')
        messages_element_text = messages_element.text
        self.assertEqual('Brak wiadomości', messages_element_text,
                         f'Expected login button text differ from actual: {messages_element_text}')


if __name__ == "__main__":
    unittest.main()
